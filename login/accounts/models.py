from django.db import models
from django.contrib.auth.models import User

class Product(models.Model):
    title = models.CharField(max_length=120)
    description =models.TextField(blank=True,null=True)
    comments = models.CharField(max_length=120,null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

class Cars(models.Model):
    carname = models.CharField(max_length=120)
    description =models.TextField(blank=True,null=True)
    carcomments = models.CharField(max_length=120,null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

class Books(models.Model):
    Bookname = models.CharField(max_length=120) 
    author = models.CharField(max_length=120)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Product,Cars,Books


class ProductForm(forms.ModelForm):
    title=forms.CharField(required=False)
    description=forms.CharField(required=False)
    comments=forms.CharField(required=False)
    class Meta:
        model = Product
        fields =[
            'title',
            'description',
            'comments'

        ]

class CarForm(forms.ModelForm):
    carname=forms.CharField(required=False)
    description=forms.CharField(required=False)
    carcomments=forms.CharField(required=False)
    class Meta:
        model = Cars
        fields =[
            'carname',
            'description',
            'carcomments'
        ]

class BookForm(forms.ModelForm):
    Bookname=forms.CharField(required=False)
    author=forms.CharField(required=False)
    class Meta:
        model = Books
        fields =[
            'Bookname',
            'author'
        ]


class CreateUserForm(UserCreationForm):
    class Meta:
        model=User
        fields=['username','email','password1','password2']

from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from .forms import ProductForm,CarForm,BookForm,CreateUserForm
from django.contrib.auth import authenticate, login, logout
from .models import Product,Cars,Books
# import logging 
# logger = logging.getLogger(__name__)

# Create your views here.
def product_create_view(request):
    if request.method == 'POST':
        
        try:
            form = ProductForm(instance=request.user.product)
        except:
            form = ProductForm(request.POST or None)

        if form.is_valid():
            Product=form.save(commit=False)
            Product.user=request.user
            Product.save()
        else:    
            newform = CarForm(request.POST,instance=request.user.product)
            newform.save()
        if 'add_text' in request.POST:
            return redirect('logout') 
        else:
            return redirect('form2')
    else:
        
        try:
            form = ProductForm(instance=request.user.product or None)
        except:
            form=ProductForm(request.POST or None)
    return render(request,"form.html",{'form':form})  
    




def Book_create_view(request):
    if request.method == 'POST':
            try:
                form = BookForm(instance=request.user.books)
            except:
                form = BookForm(request.POST or None)

            if form.is_valid():
                Books=form.save(commit=False)
                Books.user=request.user
                Books.save()
            else:    
                newform = BookForm(request.POST,instance=request.user.books)
                newform.save()
                
            if 'add_text' in request.POST:
                return redirect("logout")
            elif 'back' in request.POST:
                return redirect('form2')
            else:
                return redirect('dashboard')
    else:
        try:
            form = BookForm(instance=request.user.books or None)
        except:
            form=BookForm(request.POST or None)
        context={'form':form}


    return render(request,"form3.html",{'form':form})
    
def Car_create_view(request):
    if request.method == 'POST':
            try:
                form = CarForm(instance=request.user.cars)
            except:
                form = CarForm(request.POST or None)

            if form.is_valid():
                Cars=form.save(commit=False)
                Cars.user=request.user
                Cars.save()
            else:
                newform = CarForm(request.POST,instance=request.user.cars)
                newform.save()
            if 'add_text' in request.POST:
                return redirect("logout")
            elif 'back' in request.POST:
                return redirect('form')
            else:
                return redirect('form3')
            
    else:
        try:
            form = CarForm(instance=request.user.cars or None)
        except:
            form=CarForm(request.POST or None)
        
    


    return render(request,"form2.html",{'form':form})
        
def indexView(request):
    return render(request,'index.html')

def logoutview(request):
    logout(request)
    return redirect('login_url')

def login_view(request):
    context={}
    if request.method=='POST':
        username=request.POST.get('username')
        password=request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # if (user.product.title is None) or (user.product.description is None) or (user.product.comments is None):
            #     return redirect('form')   
            # elif (user.cars.carname is None) or (user.cars.description is None) or (user.cars.carcomments is None):
            #     return redirect('form2')
            # elif (user.books.Bookname is None) or (user.books.author is None):
            #     return redirect('form3')
            # else:
            #     return redirect('dashboard')
            try:
                form = BookForm(instance=request.user.books)
            except:
                try:
                    form = CarForm(instance=request.user.cars)
                except:
                    return redirect('form')
                return redirect('form2')        
            return redirect('form3')
    else:
        # Return an 'invalid login' error message.
        ...
    context={}
    return render(request,"registration/login.html",context)   

def dashboardView(request):
    if request.method=="POST":
        if 'add_text' in request.POST:
            return redirect('logout') 
        else:
            return redirect('form3')

    return render(request,'dashboard.html')    

def registerView(request):
    if request.method=="POST":
        form=CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            new_user = authenticate(username=form.cleaned_data['username'],password=form.cleaned_data['password1'],)
            login(request, new_user)
            return redirect('form')
    else:
        form=CreateUserForm()   
    return render(request,'registration/register.html',{'form':form})
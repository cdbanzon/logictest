from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Product
# import logging


logger = logging.getLogger(_name_)
@receiver(post_save, sender=User)
def create_product(sender, instance, created, **kwargs):
    if created:
        product=Product.objects.create(user=instance)
        product.save()

@receiver(post_save, sender=User)
def save_product(sender, instance, **kwargs):
    instance.profile.save()
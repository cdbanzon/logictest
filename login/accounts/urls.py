from django.urls import path
from . import views
from django.contrib.auth.views import LoginView,LogoutView

urlpatterns=[
    path('',views.indexView,name="home"),
    path('dashboard/',views.dashboardView,name="dashboard"),
    path('login/',views.login_view,name="login_url"),
    path('register/',views.registerView,name="register_url" ),
    path('logout/',LogoutView.as_view(),name="logout"),
    path('form/',views.product_create_view,name="form"),
    path('form2/',views.Car_create_view,name="form2"),
    path('form3/',views.Book_create_view,name="form3"),
    # path('connection/',views.formView,name="loginform")
]   
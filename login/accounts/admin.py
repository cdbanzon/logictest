from django.contrib import admin
from .models import Product,Cars,Books

# Register your models here.


admin.site.register(Product)
admin.site.register(Cars)
admin.site.register(Books)
